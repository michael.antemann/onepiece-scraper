import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppState } from '../store/app-state';
import {
  clearChapterDownloadQueue,
  loadChaptersRequest,
  processChapterDownloadQueue,
  setChapterDownloadQueue,
} from './../store/chapters/chapters.actions';
import {
  selectAllChapters,
  selectDownloadStatus,
  selectEndChapter,
  selectStartChapter,
} from './../store/chapters/chapters.selectors';
import { setDownloadPath } from './../store/config/config.actions';
import { selectDownloadPath } from './../store/config/config.selectors';

@Component({
  selector: 'app-main',
  templateUrl: 'main.component.html',
  styleUrls: ['main.component.scss'],
})
export class MainComponent implements OnInit {
  downloadPath$ = this.store.pipe(select(selectDownloadPath));
  chapters$ = this.store.pipe(select(selectAllChapters));
  downloadStatus$ = this.store.pipe(select(selectDownloadStatus));
  startChapter$ = this.store.pipe(select(selectStartChapter));
  endChapter$ = this.store.pipe(select(selectEndChapter));

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.dispatch(loadChaptersRequest());
  }

  setPath(downloadPath: string): void {
    this.store.dispatch(setDownloadPath({ downloadPath }));
  }

  download(): void {
    this.store.dispatch(processChapterDownloadQueue());
  }

  addToQueue({ start, end }): void {
    const chapters = [];
    for (let chapter = start; chapter <= end; chapter++) {
      chapters.push(chapter);
    }
    this.store.dispatch(setChapterDownloadQueue({ chapters }));
    this.store.dispatch(processChapterDownloadQueue());
  }

  stop(): void {
    this.store.dispatch(clearChapterDownloadQueue());
  }

  refresh(): void {
    this.store.dispatch(loadChaptersRequest());
  }
}
