import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-main-range-input',
  templateUrl: './main-range-input.component.html',
  styleUrls: ['./main-range-input.component.scss'],
})
export class MainRangeInputComponent implements OnInit, OnChanges {
  rangeForm = this.fb.group({ start: null, end: null });

  @Input()
  start: number;

  @Input()
  end: number;

  @Input()
  disabled = false;

  @Output()
  addToQueue = new EventEmitter();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    // TODO: JUST USE NGXS FORM STORAGE PLUGIN FOR THIS!!!!!
    const { start, end } = this;
    this.rangeForm.patchValue({ start, end }, { emitEvent: false });
  }

  submit(): void {
    const { start, end } = this.rangeForm.value;
    this.addToQueue.emit({ start, end });
  }
}
