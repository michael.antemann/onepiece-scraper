import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { ChaptersModule } from '../chapters/chapters.module';
import { SharedModule } from './../shared/shared.module';
import { MainControlsComponent } from './main-controls/main-controls.component';
import { MainRangeInputComponent } from './main-range-input/main-range-input.component';
import { MainComponent } from './main.component';

@NgModule({
  imports: [
    ChaptersModule,
    SharedModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  declarations: [MainComponent, MainControlsComponent, MainRangeInputComponent],
  exports: [MainComponent],
})
export class MainModule {}
