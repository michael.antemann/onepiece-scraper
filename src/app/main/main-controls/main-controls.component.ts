import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-main-controls',
  templateUrl: './main-controls.component.html',
  styleUrls: ['./main-controls.component.scss'],
})
export class MainControlsComponent implements OnInit {
  @Output()
  download = new EventEmitter();

  @Output()
  stop = new EventEmitter();

  @Output()
  refresh = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}
}
