import { Component, Renderer2 } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { selectTheme } from './store/config/config.selectors';
import { Theme } from './store/config/theme.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  private unsubscribe$ = new Subject();
  constructor(
    private translate: TranslateService,
    private store: Store,
    private renderer: Renderer2
  ) {
    this.translate.use('de');

    this.store
      .pipe(
        select(selectTheme),
        takeUntil(this.unsubscribe$),
        tap((newTheme) => {
          for (const themeName of Object.values(Theme)) {
            this.renderer.removeClass(document.body, `theme-${themeName}`);
          }
          this.renderer.addClass(document.body, `theme-${newTheme}`);
        })
      )
      .subscribe();
  }
}
