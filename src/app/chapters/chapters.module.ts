import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { SharedModule } from './../shared/shared.module';
import { ChapterListComponent } from './chapter-list/chapter-list.component';
import { ChapterStatusComponent } from './chapter-status/chapter-status.component';
import { ChapterTableComponent } from './chapter-table/chapter-table.component';

@NgModule({
  imports: [
    SharedModule,
    MatListModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
  ],
  declarations: [
    ChapterListComponent,
    ChapterTableComponent,
    ChapterStatusComponent,
  ],
  exports: [ChapterListComponent, ChapterTableComponent],
})
export class ChaptersModule {}
