import {
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Chapter } from '../../store/chapters/models/chapter';

@Component({
  selector: 'app-chapter-table',
  templateUrl: './chapter-table.component.html',
  styleUrls: ['./chapter-table.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ChapterTableComponent implements OnInit, OnChanges, AfterViewInit {
  @Input()
  chapters: Chapter[];

  @Input()
  downloadStatus: number;

  displayedColumns = ['id', 'status'];

  dataSource = new MatTableDataSource<Chapter>([]);

  @ViewChild(MatSort)
  sort: MatSort;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.chapters) {
      this.dataSource.data = this.chapters;
    }
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
