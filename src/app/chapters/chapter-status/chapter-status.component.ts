import { Component, Input, OnInit } from '@angular/core';
import { ChapterStatus } from './../../store/chapters/models/chapter-status.enum';

@Component({
  selector: 'app-chapter-status',
  templateUrl: './chapter-status.component.html',
  styleUrls: ['./chapter-status.component.scss'],
})
export class ChapterStatusComponent implements OnInit {
  @Input()
  status: ChapterStatus;

  @Input()
  downloadStatus: number;

  statuses = ChapterStatus;

  constructor() {}

  ngOnInit(): void {}
}
