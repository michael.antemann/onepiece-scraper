import { ChaptersState } from './chapters/chapters.reducers';
import { ConfigState } from './config/config.reducers';
export interface AppState {
  config: ConfigState;
  chapters: ChaptersState;
}
