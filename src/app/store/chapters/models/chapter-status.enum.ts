export enum ChapterStatus {
  generating = '0_generating',
  downloading = '1_downloading',
  waiting = '2_waiting',
  loaded = '3_loaded',
}
