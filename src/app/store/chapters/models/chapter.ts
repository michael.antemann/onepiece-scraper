import { ChapterStatus } from './chapter-status.enum';
export class Chapter {
  id: string;
  status: ChapterStatus;
}
