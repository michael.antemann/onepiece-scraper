import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import {
  clearChapterDownloadQueue,
  generateChapterPdfSuccess,
  loadChaptersSuccess,
  setChapterDownloadQueue,
  storeChapterPageError,
  storeChapterPageRequest,
  storeChapterPageSuccess,
  storeChapterRequest,
  storeChapterSuccess,
} from './chapters.actions';
import { Chapter } from './models/chapter';
import { ChapterStatus } from './models/chapter-status.enum';
export const featureKey = 'chapters';

interface ChaptersStateDefinition {
  pageDownloadQueue: number;
  maxPage: number;
  chapterDownloadQueue: string[];
  startChapter: number;
  endChapter: number;
}

export interface ChaptersState
  extends EntityState<Chapter>,
    ChaptersStateDefinition {}

export const adapter: EntityAdapter<Chapter> = createEntityAdapter<Chapter>();

export const initialState: ChaptersState =
  adapter.getInitialState<ChaptersStateDefinition>({
    pageDownloadQueue: 0,
    chapterDownloadQueue: [],
    maxPage: 25,
    startChapter: 989,
    endChapter: 990,
    // additional entity state properties
  });

export const chaptersReducer = createReducer(
  initialState,
  on(
    storeChapterRequest,
    (state: ChaptersState, { chapter }): ChaptersState =>
      adapter.updateOne(
        {
          id: chapter,
          changes: { status: ChapterStatus.downloading },
        },
        {
          ...state,
          pageDownloadQueue: 0,
        }
      )
  ),
  on(
    storeChapterPageRequest,
    (state: ChaptersState, { chapter, page }): ChaptersState => ({
      ...state,
      pageDownloadQueue: state.pageDownloadQueue + 1,
    })
  ),
  on(
    storeChapterPageSuccess,
    storeChapterPageError,
    (state: ChaptersState, { chapter, page }): ChaptersState => ({
      ...state,
      pageDownloadQueue: state.pageDownloadQueue - 1,
    })
  ),
  on(
    storeChapterSuccess,
    (state: ChaptersState, { chapter }): ChaptersState =>
      adapter.updateOne(
        { id: chapter, changes: { status: ChapterStatus.generating } },
        {
          ...state,
          chapterDownloadQueue: state.chapterDownloadQueue.filter(
            (item) => item !== chapter
          ),
        }
      )
  ),
  on(
    generateChapterPdfSuccess,
    (state: ChaptersState, { chapter }): ChaptersState =>
      adapter.updateOne(
        { id: chapter, changes: { status: ChapterStatus.loaded } },
        {
          ...state,
        }
      )
  ),
  on(
    loadChaptersSuccess,
    (state: ChaptersState, { chapters }): ChaptersState =>
      adapter.setAll(chapters, { ...state })
  ),
  on(
    setChapterDownloadQueue,
    (state: ChaptersState, { chapters }): ChaptersState =>
      adapter.upsertMany(
        chapters.map((chapter) => ({
          id: String(chapter).padStart(4, '0'),
          status: ChapterStatus.waiting,
        })),
        {
          ...state,
          chapterDownloadQueue: chapters.map((chapter) =>
            String(chapter).padStart(4, '0')
          ),
        }
      )
  ),
  on(
    clearChapterDownloadQueue,
    (state: ChaptersState): ChaptersState => ({
      ...state,
      chapterDownloadQueue: [],
    })
  )
);

export function reducer(state: ChaptersState, action: Action): ChaptersState {
  return chaptersReducer(state, action);
}

export const { selectIds, selectEntities, selectAll, selectTotal } =
  adapter.getSelectors();
