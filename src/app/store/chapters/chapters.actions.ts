import { createAction, props } from '@ngrx/store';
import { Chapter } from './models/chapter';

export const loadChaptersRequest = createAction(
  '[Chapters] Load Chapters Request'
);

export const loadChaptersSuccess = createAction(
  '[Chapters] Load Chapters Success',
  props<{ chapters: Chapter[] }>()
);

export const storeChapterRequest = createAction(
  '[Chapters] Store Chapter Request',
  props<{ chapter: string }>()
);

export const storeChapterSuccess = createAction(
  '[Chapters] Store Chapter Success',
  props<{ chapter: string }>()
);

export const storeChapterError = createAction(
  '[Chapters] Store Chapter Error',
  props<{ chapter: number; error: any }>()
);

export const storeChapterPageRequest = createAction(
  '[Chapters] Store Chapter Page Request',
  props<{ chapter: string; page: number }>()
);

export const storeChapterPageSuccess = createAction(
  '[Chapters] Store Chapter Page Success',
  props<{ chapter: string; page: number; filename: string }>()
);

export const storeChapterPageError = createAction(
  '[Chapters] Store Chapter Page Error',
  props<{ chapter: string; page: number }>()
);

export const generateChapterPdfRequest = createAction(
  '[Chapters] Generate Chapter PDF Request',
  props<{ chapter: string }>()
);

export const generateChapterPdfSuccess = createAction(
  '[Chapters] Generate Chapter PDF Success',
  props<{ chapter: string }>()
);

export const generateChapterPdfError = createAction(
  '[Chapters] Generate Chapter PDF Error',
  props<{ chapter: number; error: any }>()
);

export const setChapterDownloadQueue = createAction(
  '[Chapters] Set Chapter Download Queue',
  props<{ chapters: number[] }>()
);

export const clearChapterDownloadQueue = createAction(
  '[Chapters] Clear Chapter Download Queue'
);

export const processChapterDownloadQueue = createAction(
  '[Chapters] Process Chapter Download Queue'
);
