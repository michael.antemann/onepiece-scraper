import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ChaptersEffects } from './chapters.effects';
import * as fromChapters from './chapters.reducers';

@NgModule({
  imports: [
    StoreModule.forFeature(fromChapters.featureKey, fromChapters.reducer),
    EffectsModule.forFeature([ChaptersEffects]),
  ],
  exports: [],
  declarations: [],
  providers: [],
})
export class ChaptersStoreModule {}
