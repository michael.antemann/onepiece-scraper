import { Injectable } from '@angular/core';
import { combineLatest, concat, forkJoin, from, Observable, of } from 'rxjs';
import { catchError, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { FileType } from '../../core/models/file-type.enum';
import { FilesService } from './../../shared/files.service';
import { Chapter } from './models/chapter';
import { ChapterStatus } from './models/chapter-status.enum';
import { PDFDocument } from 'pdf-lib';
import { ElectronService } from '../../core/services/electron/electron.service';

@Injectable({ providedIn: 'root' })
export class ChaptersService {
  // https://manga-lesen.com/kapitel/0962/01.png
  baseUrl = 'https://manga-lesen.com/kapitel';

  constructor(
    private filesService: FilesService,
    private electronService: ElectronService
  ) {}

  loadChapters(path: string): Observable<Chapter[]> {
    return this.filesService.readFiles(path).pipe(
      map((chapters) => {
        chapters = chapters.filter((id) => id.includes('.pdf'));
        return chapters.map((id) => ({
          id: id.substring(0, id.length - 4),
          status: ChapterStatus.loaded,
        }));
      })
    );
  }

  createChapterDirectory(
    downloadPath: string,
    chapter: string
  ): Observable<boolean> {
    return this.filesService.createDirectory(
      `${downloadPath}/chapter_${chapter}`
    );
  }

  createChapterPdf(downloadPath: string, chapter: string): Observable<boolean> {
    const chapterPath = `${downloadPath}/chapter_${chapter}`;
    return combineLatest([
      from(PDFDocument.create()),
      this.filesService.readFiles(chapterPath),
    ]).pipe(
      switchMap(([pdf, files]) => {
        const requests = [];
        for (const filename of files) {
          const split = filename.split('.');
          const type = split[split.length - 1];

          const filePath = `${chapterPath}/${filename}`;
          if (type === 'png' || type === 'jpg') {
            const page = pdf.addPage();
            requests.push(
              this.filesService.readFile(filePath).pipe(
                switchMap((file) =>
                  from(
                    type === 'png' ? pdf.embedPng(file) : pdf.embedJpg(file)
                  ).pipe(
                    catchError((error) => {
                      console.log({ error });
                      throw new Error('File is not a PNG');
                    })
                  )
                ),
                tap((image) => {
                  console.log(filename);
                  const scaled = image.scaleToFit(
                    page.getWidth(),
                    page.getHeight()
                  );
                  const { width, height } = scaled;
                  page.drawImage(image, {
                    width,
                    height,
                  });
                })
              )
            );
          }
        }
        return concat(...requests).pipe(
          switchMap(() =>
            from(pdf.save()).pipe(
              switchMap((pdfFile) =>
                from(
                  this.electronService.fs.promises.writeFile(
                    `${downloadPath}/${chapter}.pdf`,
                    pdfFile
                  )
                ).pipe(
                  map(() => true),
                  tap(() => console.log('1 pdf finished'))
                )
              )
            )
          )
        );
      })
    );
  }

  storeChapterPage(
    downloadPath: string,
    { chapter, page }: { chapter: string; page: number }
  ): Observable<string> {
    const type: FileType = FileType.png;
    const pageLeadingZero = page.toString().length === 1 ? `0${page}` : page;

    const url = `${this.baseUrl}/${chapter}/${pageLeadingZero}.${type}?v2`;
    const filename = `${pageLeadingZero}.${type}`;
    const storePath = `${downloadPath}/chapter_${chapter}/${filename}`;

    return this.filesService.downloadFile(url).pipe(
      mergeMap((blob) =>
        this.filesService.storeFile(storePath, blob).pipe(map(() => filename))
      ),
      catchError(() => {
        const jpgUrl = url.replace(FileType.png, FileType.jpg);
        const jpgStorePath = storePath.replace(FileType.png, FileType.jpg);
        return this.filesService
          .downloadFile(jpgUrl)
          .pipe(
            mergeMap((blob) =>
              this.filesService
                .storeFile(jpgStorePath, blob)
                .pipe(map(() => filename))
            )
          );
      })
    );
  }
}
