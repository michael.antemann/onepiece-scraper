import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import {
  catchError,
  concatMap,
  filter,
  map,
  mergeMap,
  withLatestFrom,
} from 'rxjs/operators';
import { AppState } from '../app-state';
import { FilesService } from './../../shared/files.service';
import { setDownloadPath } from './../config/config.actions';
import { selectDownloadPath } from './../config/config.selectors';
import {
  generateChapterPdfRequest,
  generateChapterPdfSuccess,
  loadChaptersRequest,
  loadChaptersSuccess,
  processChapterDownloadQueue,
  storeChapterPageError,
  storeChapterPageRequest,
  storeChapterPageSuccess,
  storeChapterRequest,
  storeChapterSuccess,
} from './chapters.actions';
import {
  selectChapterDownloadQueue,
  selectPageDownloadQueue,
} from './chapters.selectors';
import { ChaptersService } from './chapters.service';

@Injectable()
export class ChaptersEffects {
  maxPage = 20;

  loadChaptersRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadChaptersRequest),
      withLatestFrom(this.store.pipe(select(selectDownloadPath))),
      filter(
        ([action, downloadPath]) => downloadPath && downloadPath.length > 0
      ),
      mergeMap(([action, downloadPath]) =>
        this.chaptersService
          .loadChapters(downloadPath)
          .pipe(map((chapters) => loadChaptersSuccess({ chapters })))
      )
    )
  );

  downloadPathChange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setDownloadPath),
      mergeMap(() => [loadChaptersRequest()])
    )
  );

  storeChapterRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(storeChapterRequest),
      withLatestFrom(this.store.pipe(select(selectDownloadPath))),
      concatMap(([{ chapter }, downloadPath]) => {
        // Create Chapter Directory
        return this.chaptersService
          .createChapterDirectory(downloadPath, chapter)
          .pipe(
            mergeMap(() => {
              const actions = [];
              for (let page = 1; page <= this.maxPage; page++) {
                actions.push(storeChapterPageRequest({ chapter, page }));
              }
              return actions;
            })
          );
      })
    )
  );

  storeChaptersPageRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(storeChapterPageRequest),
      withLatestFrom(this.store.pipe(select(selectDownloadPath))),
      mergeMap(([{ chapter, page }, downloadPath]) =>
        this.chaptersService
          .storeChapterPage(downloadPath, { chapter, page })
          .pipe(
            map((filename) =>
              storeChapterPageSuccess({ chapter, page, filename })
            ),
            catchError(() => of(storeChapterPageError({ chapter, page })))
          )
      )
    )
  );

  storeChaptersPageFinalized$ = createEffect(() =>
    this.actions$.pipe(
      ofType(storeChapterPageSuccess, storeChapterPageError),
      withLatestFrom(this.store.pipe(select(selectPageDownloadQueue))),
      filter(
        ([{ chapter, page }, pageDownloadQueue]) => pageDownloadQueue === 0
      ),
      mergeMap(([{ chapter, page }, pageDownloadQueue]) => {
        return [storeChapterSuccess({ chapter })];
      })
    )
  );

  storeChapterSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(storeChapterSuccess),
      mergeMap(({ chapter }) => {
        return [generateChapterPdfRequest({ chapter })];
      })
    )
  );

  processChapterDownloadQueue$ = createEffect(() =>
    this.actions$.pipe(
      ofType(processChapterDownloadQueue),
      withLatestFrom(this.store.pipe(select(selectChapterDownloadQueue))),
      map(([action, chapterDownloadQueue]) => chapterDownloadQueue),
      filter((chapterDownloadQueue) => chapterDownloadQueue.length !== 0),
      mergeMap((chapterDownloadQueue) => {
        const [chapter] = chapterDownloadQueue;
        return [storeChapterRequest({ chapter })];
      })
    )
  );

  generateChapterPdf$ = createEffect(() =>
    this.actions$.pipe(
      ofType(generateChapterPdfRequest),
      withLatestFrom(this.store.pipe(select(selectDownloadPath))),
      concatMap(([{ chapter }, downloadPath]) => {
        return this.chaptersService
          .createChapterPdf(downloadPath, chapter)
          .pipe(
            concatMap(() =>
              this.filesService
                .deleteFolder(`${downloadPath}/chapter_${chapter}`)
                .pipe(map(() => generateChapterPdfSuccess({ chapter })))
            )
          );
      })
    )
  );

  generateChapterPdfSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(generateChapterPdfSuccess),
      mergeMap(() => {
        return [processChapterDownloadQueue()];
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private chaptersService: ChaptersService,
    private filesService: FilesService
  ) {}
}
