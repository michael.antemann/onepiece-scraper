import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from '../app-state';
import * as fromChapters from './chapters.reducers';
import { ChaptersState } from './chapters.reducers';
export const selectChaptersState = createFeatureSelector<
  ChaptersState
>(fromChapters.featureKey);

export const selectChaptersIds = createSelector(
  selectChaptersState,
  fromChapters.selectIds
);

export const selectAllChapters = createSelector(
  selectChaptersState,
  fromChapters.selectAll
);

export const selectChaptersEntities = createSelector(
  selectChaptersState,
  fromChapters.selectEntities
);

export const selectChaptersTotal = createSelector(
  selectChaptersState,
  fromChapters.selectTotal
);

export const selectPageDownloadQueue = createSelector(
  selectChaptersState,
  (state) => state.pageDownloadQueue
);

export const selectChapterDownloadQueue = createSelector(
  selectChaptersState,
  (state) => state.chapterDownloadQueue
);

export const selectMaxPage = createSelector(
  selectChaptersState,
  (state) => state.maxPage
);

export const selectDownloadStatus = createSelector(
  selectPageDownloadQueue,
  selectMaxPage,
  (downloadQueue, maxPage) => maxPage / downloadQueue
);

export const selectStartChapter = createSelector(
  selectChaptersState,
  (state) => state.startChapter
);

export const selectEndChapter = createSelector(
  selectChaptersState,
  (state) => state.endChapter
);
