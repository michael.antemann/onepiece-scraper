import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../../environments/environment';
import { ChaptersStoreModule } from './chapters/chapters-store.module';
import { ConfigStoreModule } from './config/config-store.module';

@NgModule({
  imports: [
    StoreModule.forRoot({}, {}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      maxAge: 250,
      logOnly: environment.production,
    }),
    ConfigStoreModule,
    ChaptersStoreModule,
  ],
  exports: [],
  declarations: [],
  providers: [],
})
export class RootStoreModule {}
