import { createAction, props } from '@ngrx/store';
import { Theme } from './theme.enum';

export const setTheme = createAction(
  '[App] Set Theme',
  props<{ theme: Theme }>()
);

export const setDownloadPath = createAction(
  '[App] Set Download Path',
  props<{ downloadPath: string }>()
);
