import { Action, createReducer, on } from '@ngrx/store';
import { LocalStorageKeys } from './../../core/models/localstorage-keys.enum';
import { setDownloadPath, setTheme } from './config.actions';
import { Theme } from './theme.enum';
export const configFeatureKey = 'config';

export interface ConfigState {
  theme: Theme | string;
  downloadPath: string;
}

export const initialState: ConfigState = {
  theme: localStorage.getItem(LocalStorageKeys.THEME) || Theme['dark-blue'],
  downloadPath: localStorage.getItem(LocalStorageKeys.DOWNLOAD_PATH) || null,
};

const configReducer = createReducer(
  initialState,
  on(
    setTheme,
    (state: ConfigState, { theme }): ConfigState => ({ ...state, theme })
  ),
  on(
    setDownloadPath,
    (state: ConfigState, { downloadPath }): ConfigState => ({
      ...state,
      downloadPath,
    })
  )
);

export function reducer(state: ConfigState, action: Action): ConfigState {
  return configReducer(state, action);
}
