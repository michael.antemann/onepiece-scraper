import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ConfigEffects } from './config.effects';
import * as fromConfig from './config.reducers';

@NgModule({
  imports: [
    StoreModule.forFeature(fromConfig.configFeatureKey, fromConfig.reducer),
    EffectsModule.forFeature([ConfigEffects]),
  ],
  exports: [],
  declarations: [],
  providers: [],
})
export class ConfigStoreModule {}
