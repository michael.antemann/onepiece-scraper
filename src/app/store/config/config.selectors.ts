import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from '../app-state';
import { configFeatureKey, ConfigState } from './config.reducers';

export const selectConfigState = createFeatureSelector< ConfigState>(
  configFeatureKey
);

export const selectTheme = createSelector(
  selectConfigState,
  (state: ConfigState) => state.theme
);

export const selectDownloadPath = createSelector(
  selectConfigState,
  (state: ConfigState) => state.downloadPath
);
