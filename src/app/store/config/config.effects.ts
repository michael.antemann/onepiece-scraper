import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { LocalStorageKeys } from './../../core/models/localstorage-keys.enum';
import { setDownloadPath, setTheme } from './config.actions';

@Injectable()
export class ConfigEffects {
  setDownloadPath$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(setDownloadPath),
        tap(({ downloadPath }) => {
          localStorage.setItem(LocalStorageKeys.DOWNLOAD_PATH, downloadPath);
        })
      ),
    { dispatch: false }
  );

  setTheme$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(setTheme),
        tap(({ theme }) => {
          localStorage.setItem(LocalStorageKeys.THEME, theme);
        })
      ),
    { dispatch: false }
  );

  constructor(private actions$: Actions) {}
}
