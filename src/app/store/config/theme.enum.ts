export enum Theme {
  teal = 'teal',
  indigo = 'indigo',
  blue = 'blue',
  'dark-blue' = 'dark-blue',
  red = 'red',
  'dark-red' = 'dark-red',
}
