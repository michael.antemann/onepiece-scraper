export enum LocalStorageKeys {
  DOWNLOAD_PATH = 'DOWNLOAD_PATH',
  THEME = 'THEME',
}
