import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { SharedModule } from './../shared/shared.module';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
@NgModule({
  imports: [SharedModule, MatToolbarModule, MatIconModule, MatButtonModule],
  declarations: [ToolbarComponent],
  exports: [ToolbarComponent],
})
export class CoreModule {}
