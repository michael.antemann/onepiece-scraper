import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { TranslateModule } from '@ngx-translate/core';
import { PathInputComponent } from './path-input/path-input.component';
import { ThemeMenuComponent } from './theme-menu/theme-menu.component';

const materialModules = [MatButtonModule, MatIconModule, MatMenuModule];

const importExport = [
  CommonModule,
  TranslateModule,
  ReactiveFormsModule,
  FormsModule,
  MatFormFieldModule,
  MatInputModule,
  FlexLayoutModule,
];

@NgModule({
  imports: [...importExport, ...materialModules],
  declarations: [ThemeMenuComponent, PathInputComponent],
  exports: [
    ...importExport,
    // Components
    PathInputComponent,
    ThemeMenuComponent,
  ],
})
export class SharedModule {}
