import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { ElectronService } from './../../core/services/electron/electron.service';

@Component({
  selector: 'app-path-input',
  templateUrl: './path-input.component.html',
  styleUrls: ['./path-input.component.scss'],
})
export class PathInputComponent implements OnInit, OnChanges, OnDestroy {
  @Input()
  label: string;

  @Input()
  path: string;

  @Output()
  pathChange: EventEmitter<string> = new EventEmitter();

  pathControl = new FormControl('');

  unsubscribe$ = new Subject();

  constructor(private electronService: ElectronService) {}

  ngOnInit(): void {
    this.pathControl.valueChanges
      .pipe(
        takeUntil(this.unsubscribe$),
        distinctUntilChanged(),
        debounceTime(350),
        tap((path) => {
          this.pathChange.emit(path);
        })
      )
      .subscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.path) {
      this.pathControl.patchValue(this.path, { emitEvent: false });
    }
  }

  async openPathDialog(): Promise<void> {
    const result = await this.electronService.ipcRenderer.invoke(
      'open-path-dialog',
      this.path
    );
    const [path] = result.filePaths;
    console.log(path);
    this.pathChange.emit(path);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
