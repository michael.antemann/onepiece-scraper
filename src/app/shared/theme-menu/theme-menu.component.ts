import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Theme } from '../../store/config/theme.enum';
import { setTheme } from '../../store/config/config.actions';
import { selectTheme } from '../../store/config/config.selectors';
@Component({
  selector: 'app-theme-menu',
  templateUrl: './theme-menu.component.html',
  styleUrls: ['./theme-menu.component.scss'],
})
export class ThemeMenuComponent implements OnInit {
  theme$ = this.store.pipe(select(selectTheme));

  themes = Theme;

  constructor(private store: Store) {}

  ngOnInit(): void {}

  setTheme(theme: Theme): void {
    this.store.dispatch(setTheme({ theme }));
  }
}
