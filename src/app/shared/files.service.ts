import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, fromEvent, Observable, of } from 'rxjs';
import { first, map, mergeMap } from 'rxjs/operators';
import { ElectronService } from './../core/services/electron/electron.service';

@Injectable({ providedIn: 'root' })
export class FilesService {
  constructor(
    private http: HttpClient,
    private electronService: ElectronService
  ) {}

  downloadFile(url: string): Observable<Blob> {
    return this.http.get(url, {
      responseType: 'blob',
    });
  }

  createDirectory(path: string): Observable<boolean> {
    if (!this.electronService.fs.existsSync(path)) {
      return from(this.electronService.fs.promises.mkdir(path)).pipe(
        map(() => true)
      );
    } else {
      return of(true);
    }
  }

  storeFile(path: string, blob: Blob): Observable<boolean> {
    const fileReader = new FileReader();
    const fileReader$ = fromEvent(fileReader, 'load').pipe(
      mergeMap((event: ProgressEvent<FileReader>) => {
        const bytes = new Uint8Array((event.target as any).result);
        return from(
          this.electronService.fs.promises.writeFile(path, bytes)
        ).pipe(map(() => true));
      }),
      first()
    );

    fileReader.readAsArrayBuffer(blob);

    return fileReader$;
  }

  readFiles(path: string): Observable<string[]> {
    return from(this.electronService.fs.promises.readdir(path));
  }

  readFile(path: string): Observable<Buffer> {
    return from(this.electronService.fs.promises.readFile(path));
  }

  deleteFolder(path: string): Observable<boolean> {
    return from(
      this.electronService.fs.promises.rmdir(path, { recursive: true })
    ).pipe(map(() => true));
  }
}
